package com.classpath.customparititioner.paritioner;

import com.classpath.customparititioner.model.CustomerType;
import com.classpath.customparititioner.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.PartitionInfo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class CustomPartitioner implements Partitioner {
    @Override
    public int partition(String topic, Object key, byte[] bytes, Object order, byte[] bytes1, Cluster cluster) {
        log.info("Came inside the CustomPartioner method :: ");
        List<PartitionInfo> partitionInfos = cluster.availablePartitionsForTopic(topic);
        int totalParitions = partitionInfos.size();
        log.info("The number of partitions :: {}", totalParitions);
        Order payload = (Order)order;
        if (payload.getCustomerType() == CustomerType.PLATINUM){
            return 1;
        }
        //perform the mod opeartor on the number of partitions
        return 0;
    }

    @Override
    public void close() {
        //custom logic to call before closing the paritions. Called only once
    }

    @Override
    public void configure(Map<String, ?> map) {
        //has access to the configuration
    }
}