package com.classpath.customparititioner.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
public class Order {
    private Long orderId;
    private String customerName;
    private CustomerType customerType;
    private String date;
    private double price;
}
