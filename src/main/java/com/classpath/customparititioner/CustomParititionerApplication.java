package com.classpath.customparititioner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomParititionerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomParititionerApplication.class, args);
	}

}
