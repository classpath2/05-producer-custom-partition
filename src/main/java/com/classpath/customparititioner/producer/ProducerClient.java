package com.classpath.customparititioner.producer;

import com.classpath.customparititioner.model.CustomerType;
import com.classpath.customparititioner.model.Order;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

import java.time.LocalDate;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class ProducerClient implements CommandLineRunner {

    private final KafkaTemplate<Long, Order> kafkaTemplate;
    @Override
    public void run(String... args) throws Exception {
        log.info("Inside the producer client :::: start");
        Order order = Order.builder().orderId(12345L).customerName("Ramesh").price(25000).date(LocalDate.now().toString()).customerType(CustomerType.PLATINUM).build();
        ProducerRecord<Long, Order> producerRecord = new ProducerRecord<>("orders", order.getOrderId(), order);
        this.kafkaTemplate.send(producerRecord);
    }
}