package com.classpath.customparititioner.model;

public enum CustomerType {
    REGULAR,
    GOLD,
    PLATINUM
}
